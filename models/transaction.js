/** 
*  Order Line Item model
*  Describes the characteristics of each attribute in an order line item - one entry on a customer's order.
*
* @author Mohammad Farheen <s534800@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const TransactionSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  transactionType: { type: String, required: true, default: 'D' },
  transactionDate: { type: Date, required: true },
  transactionAmt: { type: Number, required: true, default: '1000' },
  
})

module.exports = mongoose.model('Transaction', TransactionSchema)

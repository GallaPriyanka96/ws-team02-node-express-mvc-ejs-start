/** 
*  Customer model
*  Describes the characteristics of each attribute in a customer resource.
*
* @author Priyanka Galla <s534884@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const UsersSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  email: { type: String, required: true, unique: true },
  firstName: { type: String, required: true, default: 'firstName' },
  lastName: { type: String, required: true, default: 'lastName' },
  addressLine1: { type: String, required: true, default: 'addressLine1' },
  addressLine2: { type: String, required: false, default: '' },
  city: { type: String, required: true, default: 'Florida' },
  state: { type: String, required: true, default: 'FL' },
  zipcode: { type: String, required: true, default: '32003' },
  country: { type: String, required: true, default: 'USA' }
})

module.exports = mongoose.model('Users', UsersSchema)

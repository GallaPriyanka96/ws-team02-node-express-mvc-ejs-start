/** 
*  Order model
*  Describes the characteristics of each attribute in an order resource.
*
* @author Nandini Doppalapudi <s534860@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  accountholdername: { type: String, required: true },
  accountType: { type: String, enum: ['NA', 'credit card', 'Savings', 'check'], required: true, default: 'Savings' },
  accountbalance: { type: Number, required: true , default: '2000'}
})

module.exports = mongoose.model('Account', AccountSchema)

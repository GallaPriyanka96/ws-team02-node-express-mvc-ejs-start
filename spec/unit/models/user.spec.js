// Jasmine provides the describe method
// Argument 1 gives a short description to the tested feature
// Argument 2 is a function that executes its expectations

// it is used for expectations

// note path to find models - up to models & unit & spec & root 

const Model = require('../../../models/user.js')

describe("Test user model", function() {

    it("creates a new model with default values", function() {
      const item = new Model();
      expect(item.zipcode).toBe('32003');
      expect(item.state).toBe('FL');
      });

});